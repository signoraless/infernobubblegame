﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeTilt : MonoBehaviour {

	public int RotateSpeed = 2;
	public float Torque = 2;
	public float RotateAngle;


	void Start () {
		
	}

	void Update () {
		
		if (Input.GetKey (KeyCode.W)) {
				transform.Rotate (15.0F * Time.deltaTime, 0.0F * Time.deltaTime, 0.0F);
		}
	
		if (Input.GetKey (KeyCode.S)) {
				transform.Rotate (-15.0F * Time.deltaTime, 0.0F, 0.0F * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.A)) {
				transform.Rotate (0.5F * Time.deltaTime, 0.0F, 0.5F);
		}

			if (Input.GetKey (KeyCode.D))
				transform.Rotate (0.5F*Time.deltaTime, 0.0F, -0.5F);
		}
}
