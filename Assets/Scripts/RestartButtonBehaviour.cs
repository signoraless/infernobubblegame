﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RestartButtonBehaviour : MonoBehaviour {

	public void NewRestartGameBtn(string restartGameLevel) {

		Scene scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
		Debug.Log("Active scene is '" + scene.name + "'.");

		UnityEngine.SceneManagement.SceneManager.LoadScene (scene.name);

	}
}
