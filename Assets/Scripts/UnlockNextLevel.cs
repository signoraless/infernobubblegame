﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockNextLevel : MonoBehaviour {

	public GameObject pSystem;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter(Collision col) {

		if (col.transform.tag == "Ball") {
			
			//Change color, for visual cue
			GetComponent<Renderer>().material.color = Color.black;
			pSystem.SetActive(true);
			StartCoroutine (stoppSystem ());

		}
	}

		IEnumerator stoppSystem() {

			yield return new WaitForSeconds(5f);
			UnityEngine.SceneManagement.SceneManager.LoadScene ("Menu");
			
		}

}
