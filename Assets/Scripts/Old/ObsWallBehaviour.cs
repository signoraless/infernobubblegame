﻿//Reference: Unity tutorial

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsWallBehaviour : MonoBehaviour {

	public bool IsExploding = false;

	public GameObject[] SpawnPrefab;
	public Transform[] SpawnLocation;

	//public int CurrentlyCollidingSameColoredBall = 0;
	//public List<Transform> AllSameColoredBallsImCollidingWith = new List<Transform>();


	// Use this for initialization
	void Start () {

		gameObject.SetActive (true);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision otherObj) {
		if (otherObj.gameObject.tag == "Ball") {
			//Destroy(gameObject,.5f);
			gameObject.SetActive (false);

		}
	}
}
