﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dance : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Alpha1)) 
		{
			GetComponent<Animator> ().SetBool ("Move1", true);		
		}
		if (Input.GetKeyUp (KeyCode.Alpha1)) 
		{
			GetComponent<Animator> ().SetBool ("Move1", false);
		}


		if (Input.GetKeyDown (KeyCode.Alpha2)) 
		{
			GetComponent<Animator> ().SetBool ("Move2", true);		
		}
		if (Input.GetKeyUp (KeyCode.Alpha2)) 
		{
			GetComponent<Animator> ().SetBool ("Move2", false);
		}
			

	}
}
