﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour {

	public float speed;
	private Rigidbody rb;

	public bool IsExploding = false;
	public int CurrentlyCollidingSameColoredBalls = 0;
	public List<Transform> AllSameColoredBallsImCollidingWith = new List<Transform>();


	// Use this for initialization
	void Start () {

		rb = GetComponent<Rigidbody>();

	}
	

	void OnCollisionEnter(Collision col) {

		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Debug.Log ("Ball has collided with" + col.transform.tag);
		GetComponent<AudioSource> ().Play ();

		if ((col.transform.tag == "Wall") || (col.transform.tag == "Ball")) {
			this.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
			Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
			rb.AddForce (movement * speed);

		}

		if (col.transform.tag == "Receiver") {
			Debug.Log ("Ball: I hit the goal post");
			Destroy (gameObject);
		}

	}

	void Explode() {

		GameObject.Find ("MainManager").GetComponent<MainManager>().TotalScore++;

		IsExploding = true;
		foreach (Transform ball in AllSameColoredBallsImCollidingWith) {
			if (ball.GetComponent<BallBehaviour> ().IsExploding == false) {
				ball.GetComponent<BallBehaviour> ().Explode ();
			}
		
		}

		Destroy(gameObject);
	
	}

}
