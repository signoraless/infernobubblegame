﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallGenerator : MonoBehaviour {

	public GameObject BallPrefab;
	public Transform SpawnPoint;
	public float RotateSpeed;

	public Material[] mat;
	//public Renderer GenRenderer;

	GameObject NewBall; 
	public float CurrentForce;
	private int chosenmaterial;

	// Use this for initialization
	void Start () {

		int chosenmaterial = (int)Random.Range (0, mat.Length);
		GetComponent<Renderer>().material = mat [chosenmaterial] ;


		NewBall = Instantiate (BallPrefab);

		NewBall.GetComponent<Transform> ().position = SpawnPoint.position;
		NewBall.GetComponent<Transform> ().rotation = SpawnPoint.rotation;
		NewBall.GetComponent<Renderer> ().material = GetComponent<Renderer>().material;

		//Color MaterialColour =GetComponent<Renderer>().material.color;
		//GetComponent<Renderer>().material.color = new Color (MaterialColour.r, MaterialColour.g, MaterialColour.b);

		//GetComponent<AudioSource> ().Play ();

		Vector3 DirectionVector = SpawnPoint.position - transform.position;
		DirectionVector = DirectionVector * CurrentForce;
		if (NewBall != null) {
				NewBall.GetComponent<Rigidbody> ().AddForce (DirectionVector);
			}
			CurrentForce = 0;
		}
			
	void Update () {
	}
}
